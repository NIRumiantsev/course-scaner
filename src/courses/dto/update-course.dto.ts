import { CourseDuration, CoursePrice } from 'courses/types';

export class UpdateCourseDto {
  readonly title?: string
  readonly direction?: string
  readonly sub_direction?: string
  readonly duration?: CourseDuration
  readonly price_info?: CoursePrice
  readonly href?: string
  readonly level?: string
  readonly image?: string
}