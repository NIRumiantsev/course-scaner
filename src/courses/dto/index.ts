import { CreateCourseDto } from 'courses/dto/create-course.dto';
import { UpdateCourseDto } from 'courses/dto/update-course.dto';

export {
  CreateCourseDto,
  UpdateCourseDto,
};