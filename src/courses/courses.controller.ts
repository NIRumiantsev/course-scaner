import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Delete,
  Put,
  HttpCode,
  HttpStatus,
  Header,
  HttpException,
} from '@nestjs/common';
import { UpdateCourseDto, CreateCourseDto } from 'courses/dto';
import { Course } from 'courses/shemas/course.schema';
import { CoursesService } from 'courses/courses.service';

@Controller('courses')
export class CoursesController {
  constructor(private readonly coursesService: CoursesService) {
  }

  @Get()
  @HttpCode(HttpStatus.FOUND)
  @Header('Cache-Control', 'none')
  getAll(): Promise<Course[]> {
    return this.coursesService.getAll().catch(() => {
      throw new HttpException('Произошла ошибка', HttpStatus.FORBIDDEN)
    });
  }

  @Get(':id')
  @HttpCode(HttpStatus.FOUND)
  @Header('Cache-Control', 'none')
  getOne(@Param('id') id: string): Promise<Course> {
    return this.coursesService.getById(id).catch(() => {
      throw new HttpException('Ошибка поиска товара', HttpStatus.FORBIDDEN)
    });
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @Header('Cache-Control', 'none')
  create(@Body() Course: CreateCourseDto): Promise<Course> {
    return this.coursesService.create(Course).catch(() => {
      throw new HttpException('Ошибка создания товара', HttpStatus.FORBIDDEN)
    });
  }

  @Delete(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  @Header('Cache-Control', 'none')
  remove(@Param('id') id: string): Promise<Course> {
    return this.coursesService.remove(id).catch(() => {
      throw new HttpException('Ошибка удаления товара', HttpStatus.FORBIDDEN)
    });
  }

  @Put(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  @Header('Cache-Control', 'none')
  update(@Body() course: UpdateCourseDto, @Param('id') id: string): Promise<Course> {
    return this.coursesService.update(id, course).catch(() => {
      throw new HttpException('Ошибка обновления товара', HttpStatus.FORBIDDEN)
    });
  }
}
