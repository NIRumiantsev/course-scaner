import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { CourseDuration, CoursePrice } from 'courses/types';

export type CourseDocument = Course & Document

@Schema()
export class Course {
  @Prop()
  title: string

  @Prop()
  direction: string

  @Prop()
  sub_direction: string

  @Prop({type: {}})
  duration: CourseDuration

  @Prop({type: {}})
  price_info: CoursePrice

  @Prop()
  href: string

  @Prop()
  level?: string

  @Prop()
  image?: string
}

export const CourseSchema = SchemaFactory.createForClass(Course)