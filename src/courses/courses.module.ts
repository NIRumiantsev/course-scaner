import { Module } from '@nestjs/common';
import { CoursesService } from 'courses/courses.service';
import { CoursesController } from 'courses/courses.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Course, CourseSchema } from 'courses/shemas/course.schema';

@Module({
  providers: [CoursesService],
  controllers: [CoursesController],
  imports: [
    MongooseModule.forFeature([
      {name: Course.name, schema: CourseSchema}
    ])
  ]
})
export class CoursesModule {
}