import { CourseDuration } from './CourseDuration';
import { CoursePrice } from './CoursePrice';

export {
  CoursePrice,
  CourseDuration
}