export type CourseDuration = {
  count: number,
  label: string
}