export type CoursePrice = {
  cost: number,
  discount_value?: number,
  discounted_price?: number,
  credit_duration?: number,
}