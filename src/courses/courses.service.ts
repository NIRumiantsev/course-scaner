import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateCourseDto } from 'courses/dto/create-course.dto';
import { Course, CourseDocument } from 'courses/shemas/course.schema';
import { UpdateCourseDto } from 'courses/dto/update-Course.dto';

@Injectable()
export class CoursesService {
  constructor(@InjectModel(Course.name) private courseModel: Model<CourseDocument>) {
  }

  async getAll(): Promise<Course[]> {
    return this.courseModel.find().exec();
  }

  async getById(id: string): Promise<Course> {
    return this.courseModel.findById(id);
  }

  async getByDirection(direction: string): Promise<Course[]> {
    const currentCourses = await this.courseModel.find().exec();
    return currentCourses.filter((course: Course) => course.direction === direction);
  }

  async create(Course: CreateCourseDto): Promise<Course> {
    const newCourse = new this.courseModel(Course);
    return newCourse.save();
  }

  async remove(id: string): Promise<Course> {
    return this.courseModel.findByIdAndRemove(id);
  }

  async update(id: string, Course: UpdateCourseDto): Promise<Course> {
    return this.courseModel.findByIdAndUpdate(id, Course, {new: true});
  }
}
