import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

export type AuthDocument = Auth & Document

@Schema()
class Auth {
  @Prop()
  token: string

  @Prop()
  uId: Types._ObjectId

  @Prop()
  expireAt: Date
}
const AuthSchema = SchemaFactory.createForClass(Auth);
AuthSchema.index({token: 1, uId: 1}, {unique: true});

export { Auth, AuthSchema }