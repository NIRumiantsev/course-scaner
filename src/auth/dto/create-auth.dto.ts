import * as mongoose from 'mongoose';

export class CreateAuthDto {
  readonly token: string
  readonly uId: mongoose.Types._ObjectId
  readonly expireAt: Date
}