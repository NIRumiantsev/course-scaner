import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from 'users/schemas/user.schema';
import { AuthUserDto } from 'users/dto';
import { UserService } from 'users/user.service';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private userService: UserService
  ) {
  }

  async validateUser(login: string, password: string): Promise<User | null> {
    const user: User = await this.userService.findByLogin(login)
    if (user && user.password === password) {
      const {password, ...secureUser} = user;
      return secureUser;
    }
    return null;
  }

  async login(user: any) {
    const payload = { id: user._doc._id };
    return {
      accessToken: this.jwtService.sign(payload)
    }
  }
}