import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CoursesModule } from 'courses/courses.module';
import { UsersModule } from 'users/user.module';
import { AuthModule } from "auth/auth.module";

@Module({
  imports: [
    CoursesModule,
    UsersModule,
    AuthModule,
    ConfigModule.forRoot({envFilePath: '.env'}),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('MONGO_CONNECT_URI'),
      }),
      inject: [ConfigService],
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
