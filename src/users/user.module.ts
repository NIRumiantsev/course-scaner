import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserService } from 'users/user.service';
import { UsersController } from 'users/users.controller';
import { User, UserSchema } from 'users/schemas/user.schema';

@Module({
  providers: [UserService],
  controllers: [UsersController],
  imports: [
    MongooseModule.forFeature([
      {name: User.name, schema: UserSchema}
    ])
  ],
  exports: [UserService]
})
export class UsersModule {}