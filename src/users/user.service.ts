import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { assignIn } from 'lodash';
import { genSalt, hash } from 'bcrypt';
import { User, UserDocument } from 'users/schemas/user.schema';
import {CallbackError, Model} from 'mongoose';
import { CreateUserDto } from "users/dto";

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {
  }

  async create(user: CreateUserDto): Promise<User> {
    const createdUser = new this.userModel(user);
    return createdUser.save();
  }

  async find(id: string): Promise<User> {
    return await this.userModel.findById(id).exec();
  }

  async findByLogin(login: string): Promise<User> {
    let currentUser: User = null;
    await this.userModel.findOne({email: login}, function(err, data){
      currentUser = data
    });
    return currentUser;
  }
}