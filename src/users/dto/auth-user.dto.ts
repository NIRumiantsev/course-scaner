export class AuthUserDto {
  readonly _id: string
  readonly firstName: string
  readonly lastName: string
  readonly role: 'TUTOR' | 'STUDENT' | 'ADMIN'
  readonly password: string
  readonly avatar?: string
  readonly phone?: string
}