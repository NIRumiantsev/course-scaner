export class CreateUserDto {
  readonly firstName: string
  readonly lastName: string
  readonly role: 'TUTOR' | 'STUDENT' | 'ADMIN'
  readonly password: string
  readonly avatar?: string
  readonly phone?: string
  readonly email?: string
}