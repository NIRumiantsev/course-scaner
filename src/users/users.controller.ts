import {
  Controller,
  Get,
  Post,
  HttpCode,
  HttpStatus,
  Header,
  Param,
  HttpException, Body
} from '@nestjs/common';
import { UserService } from 'users/user.service';
import { User } from 'users/schemas/user.schema';
import { CreateUserDto } from 'users/dto';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UserService) {
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @Header('Cache-Control', 'none')
  getOne(@Param('id') id: string): Promise<User> {
    return this.userService.find(id).catch(() => {
      throw new HttpException('Ошибка поиска пользователя', HttpStatus.FORBIDDEN)
    });
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @Header('Cache-Control', 'none')
  create(@Body() user: CreateUserDto): Promise<User> {
    return this.userService.create(user)
  }
}
