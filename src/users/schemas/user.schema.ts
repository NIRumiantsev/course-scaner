import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document

@Schema()
export class User {
  @Prop()
  firstName: string

  @Prop()
  lastName: string

  @Prop()
  email: string

  @Prop()
  role: 'TUTOR' | 'STUDENT' | 'ADMIN'

  @Prop()
  password?: string

  @Prop()
  avatar?: string

  @Prop()
  phone?: string
}
const UserSchema = SchemaFactory.createForClass(User)
UserSchema.index({email: 1, phone: 1}, {unique: true})

export { UserSchema }