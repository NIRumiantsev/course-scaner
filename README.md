## Before start

To run this app you have to copy the content of env.dist file to .env in the root directory of the project 
(cp env.dist .env) and then put the actual MongoDB uri to MONGO_CONNECT_URI

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Author

Created by Nikolai Rumiantsev (https://github.com/NIRumiantsev)


